gwmi Win32_LogicalDisk | Foreach-Object { 'Disk {0} has {1:0.0} GB space available' -f $_.Caption, ($_.FreeSpace / 1024MB) }

get-wmiobject -namespace root\WMI -computername localhost -Query "Select  CurrentTemperature from MSAcpi_ThermalZoneTemperature"

$cpu=((get-counter -counter "\processor(_total)\% processortid").CounterSamples|where {$_.InstanceName -eq "_total" }).CookedValue
$Havecpu = "CPU Exploitation: {0:0.0} %" -f $cpu
$Havecpu

$counter = New-Object Diagnostics.PerformanceCounter
$counter.CategoryName = "Memory"
$counter.CounterName = "Available MBytes"
$value = "Memory Available:" + $counter.NextValue() +";"
$value
