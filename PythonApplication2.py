from netmiko import Netmiko
import Wincmdcolor
import os
import time
from datetime import datetime
import sys



def get_device_list(file_path) :
    filehandler = open(file_path,'r') 
    lines = filehandler.readlines()
    device_list = []
    for line in lines :
        device_list.append(line.split(','))
    filehandler.close()
    return device_list

if __name__ == '__main__':
    clr = Wincmdcolor.Color()
    
    for item_device in get_device_list('list.txt'):
            print('Name: %s' % item_device[4])
            try:
                cisco1 = {
                    "host": item_device[0],
                    "username": item_device[1],
                    "password": item_device[2],
                    "device_type": item_device[3].replace('\n',''),
                    "secret": "cisco",
                }
                net_connect = Netmiko(**cisco1)
                #print(net_connect.find_prompt())
                if item_device[3].replace('\n','') == 'cisco_ios' :
                    net_connect.enable()
                    #show cpu temperature
                    
                    command = "show environment all"
                    output = net_connect.send_command(command)
                    environment_out = output.split("\n")
                    environment_index = 0
                    cpu_temperature = float (environment_out[15].split(",")[0].split(':')[1].split()[0].strip())
                    if cpu_temperature > 50:
                        return_mes = 'CPU temperature: %s Celsius, %s' %(cpu_temperature,'Heigh')
                        clr.print_red_text(return_mes)
                    else:
                        return_mes = 'CPU temperature: %s Celsius, %s' %(cpu_temperature,'Normal')
                        print(return_mes)                    

                    #show status of Interface
                    print('Status of interface:')
                    command = "show ip int brief"
                    output = net_connect.send_command(command)
                    interface_list = output.split("\n")
                    interface_index = 1
                    while interface_index <= len(interface_list) -1 :
                        item_mes = interface_list[interface_index].split();
                        return_mes = '%30s:%s' %(item_mes[0],item_mes[5])
                        if item_mes[5] == 'down':
                            clr.print_red_text(return_mes)
                        else :
                            print(return_mes)
                        interface_index += 1

                    #backup configualb
                    timestr = time.strftime('%Y-%m-%d-%H-%M-%S',time.localtime(time.time()))
                    command = 'show running'
                    filename = (u'{0}_{1}_{2}.txt'.format(item_device[0],command,timestr))
                    filepath = r'C:\Users\xipan\source\repos\PythonApplication1\/'
                    if not os.path.exists(filepath):
                        message =  "Now, I will create the %s"
                        os.makedirs(filepath)
                    save = open(filepath + filename,'w')
                    print(u'[+] executing {0} command'.format(command))
                    output = net_connect.send_command(command)
                    time.sleep(2)
                    save.write(output)
                    print(u'[+] {0} command executed,result was saved at {1}!'.format(command,filename))
                    save.close()
                elif item_device[3].replace('\n','') == 'terminal_server' :
                    command = 'powershell\r\n'    
                    command = command + 'cd C:\\Users\\xipan\\source\\repos\\PythonApplication1\\\r\n'
                    command = command + 'Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass\r\n'
                    command = command + '.\\performance.ps1\r\n'
                    net_connect.write_channel(command)
                    time.sleep(10)

                    output = net_connect.read_channel()    
                    index = output.find("Disk C: has")
                    disk_c_available = float(output[index:index + 35].split()[3].strip().replace(",","."))
                    index = output.find("Disk D: has")
                    disk_d_available = float(output[index:index + 35].split()[3].strip().replace(",","."))
                    index = output.find("Disk E: has")
                    disk_e_available = float(output[index:index + 35].split()[3].strip().replace(",","."))
                    index = output.find("Disk F: has")
                    disk_f_available = float(output[index:index + 35].split()[3].strip().replace(",","."))
                    index = output.find("CurrentTemperature")
                    index = output.index("CurrentTemperature",index)
                    cpu_temperature = float(output[index:index + 35].split(":")[1][0:5].strip().replace(",","."))
                    cpu_temperature = round( (( cpu_temperature / 10 ) - 273.15) , 2)
                    index = output.find("CPU Exploitation:")
                    cpu_exploitation = float(output[index:index + 35].split(":")[1].split("%")[0].strip().replace(",","."))
                    index = output.find("Memory Available:")
                    memory_available = float(output[index:index + 35].split(":")[1][0:4].strip().replace(",","."))
                    memory_available = round( (memory_available / 1024), 2)

                    return_mes = 'Disk available:\r\n'
                    return_mes = return_mes + '  C: %.2f GB'%(disk_c_available) + "\r\n"
                    return_mes = return_mes + '  D: %.2f GB'%(disk_d_available) + "\r\n"
                    return_mes = return_mes + '  E: %.2f GB'%(disk_e_available) + "\r\n"
                    return_mes = return_mes + '  F: %.2f GB'%(disk_f_available) + "\r\n"
                    return_mes = return_mes + 'CPU Temperature: %.2f Celsius'%(cpu_temperature) + "\r\n"
                    return_mes = return_mes + 'CPU Exploitation: %.2f %%'%(cpu_exploitation) + "\r\n"
                    return_mes = return_mes + 'Memory Available: %.2f GB'%(memory_available) + "\r\n"
                    
                    print(return_mes)

                net_connect.disconnect()  
                input()
            except Exception:
                clr.print_red_text('The device connection error')
                continue